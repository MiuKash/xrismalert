
import glob
import numpy as np
import pandas as pd
from sklearn.cluster import MiniBatchKMeans

class Select_datasets:

    def __init__(self) :
        pass



    def concat_npz(self,target_list):
        """
        npzファイルのすべてを結合する

        return: DataFrame
        """
         
#         if target_list == False:
#             npz_files = glob.glob(self.path+"/**.npz")
#             target_list = npz_files
            
        data_np = np.concatenate(list(map(self.load_npzfile,target_list)))
        return pd.DataFrame(data_np)

    def delete_0(self,data_df):
        """
        すべて０の行を取り除く、ほかの０を周りのもので補う

        data_df: Dataframe
        """
        data_df.replace(0, np.nan, inplace=True)
        data_df=data_df.dropna(axis=0,how="all")
        data_df.fillna(method="ffill")
        data_df.index = list(range(0,len(data_df)))
        return data_df

    def delete_0_gyou(self,data_df):
        """
        すべて０の行を取り除く

        data_df: Dataframe
        """
        data_df.replace(0, np.nan, inplace=True)
        data_df=data_df.dropna(axis=0,how="all")
        data_df=data_df.fillna(0)
        data_df.index = list(range(0,len(data_df)))
        return data_df

    def change_scale(self,data_df):
        """
        log に変える
        """
        return np.log10(data_df)

    def load_npzfile(self,file):
        return np.load(file)["arr_0"].T

    def train_test_dataframe(self,df_data,test_frac=0.01):
        df_test =  df_data.sample(frac=test_frac,random_state=0)
        df_train = df_data[~df_data.index.isin(df_test.index)]
        df_train.index = range(len(df_train))
        df_test.index = range(len(df_test))
        return df_train,df_test

    def train_test_dataframe_save_index(self,df_data,test_frac=0.01):
        df_test =  df_data.sample(frac=test_frac,random_state=0)
        df_train = df_data[~df_data.index.isin(df_test.index)]
        df_train = df_train.sort_index()
        df_test = df_test.sort_index()
        return df_train,df_test

class Get_datasets:

    def __init__(self):
        pass

    def get_TSNE_datasets(self,df_tsne_data_std):
        tsne = TSNE(random_state=0)
        List_tsne_result = tsne.fit_transform(df_tsne_data_std)

        MiniB = MiniBatchKMeans(init="random",n_clusters=n_cluster,random_state=0)

        df_tsne_kmeans_clusters = MiniBatchKMeans(List_tsne_result)



    



    
    

        
    