import numpy as np
np.random.seed(555)
import matplotlib.pyplot as plt
import pandas as pd
import scipy.stats as stats

class BayesianOnlineChangePointDetection(object):
    T = 0  # 観測時間
        
    def __init__(self, hazard_func, distribution):        
        self.beliefs = np.zeros((2,2))
        self.beliefs[0, 0] = 1.
        # ハザード関数と連長に関する確率密度分布は引数として与える
        self.hazard_func = hazard_func
        self.distribution = distribution
        
    def reset_params(self):
        """パラメタを初期状態にリセット
        """
        self.T = 0
        self.beliefs = np.zeros((2,2))
        # 例では初期状態で連長が必ず0になるので信念の初期状態は1.0
        self.beliefs[0, 0] = 1.

    def _expand_belief_matrix(self):
        """観測時点数に合わせて信念行列beliefを大きくする
        """
        n_row, n_col = self.beliefs.shape
        rows = np.zeros((1, 2))
        cols = np.zeros((n_row+1, 1))
        self.beliefs = np.concatenate((self.beliefs, rows), axis=0)

    def _shift_belief_matrix(self):
        """信念行列の更新
        """
        current_belief = self.beliefs[:, 0]
        self.beliefs[:, 0] = self.beliefs[:, 1]
        self.beliefs[:, 1] = 0
        return current_belief

    def _update_beliefs(self, hazard, pred_probs):
        """信念（連長分布）の更新
        """
        self._expand_belief_matrix()

        # 連長の増加確率の計算
        # （次の時点で連長が+1になる確率）
        self.beliefs[1:self.T+2, 1] = \
            self.beliefs[:self.T+1, 0] * pred_probs * (1 - hazard)

        # 変化点確率の計算
        # （次時点で連長が0になる確率）
        self.beliefs[0, 1] = \
            (self.beliefs[:self.T+1, 0] * pred_probs * hazard).sum()
        
        # 信念（連長分布）の正規化
        self.beliefs[:, 1] = self.beliefs[:, 1] / self.beliefs[:, 1].sum()
        
    def update(self, x):
        """予測分布、ハザード関数、連長分布、観測時間の更新
        """
        # 予測分布の評価
        pred_probs = self.distribution.pdf(x)

        # ハザード関数の評価
        hazard = self.hazard_func(np.arange(self.T+1))
    
        # 連長分布,信念の更新
        self._update_beliefs(hazard, pred_probs)
        
        # 十分統計量（ここではハイパーパラメタと同義）の更新
        self.distribution.update_params(x)

        # 信念が最大のindexを取得（最も可能性の高い連長を取得）
        max_belief_idx = np.where(self.beliefs[:, 0]==self.beliefs[:, 0].max())[0]
        # 現在の信念の取得
        current_belief = self._shift_belief_matrix()
    
        self.T += 1
        return max_belief_idx, current_belief
    
class StudentT(object):
    def __init__(self, mu0=0, kappa0=1, alpha0=1, beta0=1):
        """パラメタの初期化
        """
        self.mu0    = np.array([mu0])
        self.kappa0 = np.array([kappa0])
        self.alpha0 = np.array([alpha0])
        self.beta0  = np.array([beta0])
        self.reset_params()
    
    def reset_params(self):
        """パラメタを初期状態にリセット
        """
        self.muT    = self.mu0.copy()
        self.kappaT = self.kappa0.copy()
        self.alphaT = self.alpha0.copy()
        self.betaT  = self.beta0.copy()
        
    def pdf(self, x):
        """確率密度関数
        確率密度分布のある点(x)における確率を返す
        """
        return stats.t.pdf(
            x,
            loc=self.muT,
            df=2 * self.alphaT,
            scale=np.sqrt(self.betaT * (self.kappaT + 1) / (self.alphaT * self.kappaT)),
        )
        
    def update_params(self, x):
        """パラメタ（十分統計量）の更新
        それぞれのパラメタベクトルは観測点分のベクトルとなる
        """
        self.betaT = np.concatenate(
            [self.beta0, 
             (self.kappaT
              + (self.kappaT * (x - self.muT)**2) / (2 * (self.kappaT + 1)))
            ])
        self.muT = np.concatenate(
            [self.mu0, (self.kappaT * self.muT + x) / (self.kappaT + 1) ])
        self.kappaT = np.concatenate([self.kappa0, self.kappaT + 1 ])
        self.alphaT = np.concatenate([self.alpha0, self.alphaT + 0.5 ])
        
def constant_hazard(r, _lambda):
    """ハザード関数
    予測分布が指数関数の場合は観測時点のみに依存
    1/_lambda
    
    Args:
      r: 観測時間(np.ndarray or scalar)
      _lambda: ハイパーパラメタ(float)
    Returns:
      p: 変化点である確率(np.ndarray with shape = r.shape)
    """
    if isinstance(r, np.ndarray):
        shape = r.shape
    else:
        shape = 1

    # 変化点確率の計算
    probability = np.ones(shape) / _lambda
    return probability
