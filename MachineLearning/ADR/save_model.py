import numpy as np
from sklearn import svm
import pickle

class save_models:

    def __init__(self,c_name=False):
        self.c_name = c_name

    def make_filename(self,model):
        fname = str(model).split("(")[0]
        if self.c_name != False:
            fname += self.c
            
        return fname

    def save_sup_pickle(self,X,y,model):
        fname = self.make_filename(model)
        Model = model
        Model.fit(X,y)

        with open(f'{fname}.pickle', mode='wb') as f:
            pickle.dump(Model,f,protocol=2)

    def save_unsu_pickle(self,X,model):
        model_name = str(model).split("(")[0]
        Model = model
        Model.fit(X)

        with open(f'{model_name}.pickle', mode='wb') as f:
            pickle.dump(Model,f,protocol=2)

    def open_pickle(self,model):

        with open(f'{model}.pickle', mode='rb') as f:
            clf = pickle.load(f)
        return clf

    def save_data(self,data,name):
        
        with open(f'{name}.pickle', mode='w') as f:
            f.write(data)
