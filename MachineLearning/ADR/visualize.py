import numpy as np
import math
import matplotlib.pyplot as plt

class visualization:
    def __init__(self):
        pass

    def show_multiple_fig(self,x,data,plot_line_number=4,type="line",log_scale=False,xticks=True,grid=True,xlim=1000,legend=False):

        """
        x:nのリスト
        data:nのリスト
        """

        n_cluster = len(data)

        fig,ax = plt.subplots(math.ceil(n_cluster/plot_line_number),plot_line_number,figsize=(20,20))

        for i,d in enumerate(data):

            yoko = i%plot_line_number
            tate = i//plot_line_number

            if type == "line":
                ax[tate][yoko].plot(x,d,label=f"{i}")
            elif type == "scatter":
                ax[tate][yoko].scatter(x,d,label=f"{i}")
            
            ax[tate][yoko].set_xlim(0,xlim)
            
            if legend:
                ax[tate][yoko].legend()
            
            if xticks:
                ax[tate][yoko].set_xticks(x)
            
            if grid:
            
                ax[tate][yoko].grid()
            
            if log_scale:
            
                ax[tate][yoko].set_xscale("log")
                ax[tate][yoko].set_yscale("log")


    
    def show_multiple_fig_one_graph(self,x,data,type="line",xlim=1000,legend=True):

        """
        x:nのリスト
        data:nのリスト
        """

        n_cluster = len(data)

        fig,ax = plt.subplots()

        for i,d in enumerate(data):

            if type == "line":
                ax.plot(x,d,label=f"{i}")
            elif type == "scatter":
                ax.scatter(x,d,label=f"{i}")
                
        if legend:
            ax.legend()

        ax.set_xticks(x)
        ax.set_ylim([0,xlim])
        ax.grid()

    def show_multiple_figure_TwoSorce(self,x,sorce_one,sorce_two,ylim=1000,sorce1_label=False):
    
        fig,ax = plt.subplots()

        if sorce1_label == False:

            for i in sorce_one:
                ax.plot(x,i)

        else:
            for i,label in zip(sorce_one,sorce1_label):
                ax.plot(x,i,label = label)
            ax.legend()

        for i in sorce_two:
            ax.plot(x,i,color="black")
       

        ax.set_ylim(0,ylim)




    def avarange(self,target_data):
        """
        n個のクラスに分かれたデータのm個の平均化

        data:n*mのリストor dataframe
        return n*1のリスト
        """

        cluster_per_average = []

        for d in target_data:
            D = np.array(d).mean(axis=0)
            cluster_per_average.append(D)

        return cluster_per_average
