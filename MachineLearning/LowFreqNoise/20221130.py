def save_to_pickle(X,y,model):
    model_name = str(model).split("(")[0]
    Model = model
    Model.fit(X,y)
    with open(f'{model_name}.pickle', mode='wb') as f:
        pickle.dump(Model,f,protocol=2)
            
def open_pickle(model):
    with open(f'{model}.pickle', mode='rb') as f:
        clf = pickle.load(f)
    return clf


if __name__ == '__main__':
    
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    import math
    import glob
    import pickle
    from sklearn.manifold import TSNE
    from sklearn.preprocessing import StandardScaler
    
    from select_datasets import Select_datasets as my_SD
    from visualize import visualization
    myvs = visualization()
    
    
    # Constant

    f_s = 12.5*10**3
    th_s = 2
    N = 4096
    pixel_number=36
    
    f_range = f_s/th_s
    f_resolution = f_range/N
    
    #sample_data

    path="/data7/ALL/db/ns/8k"
    npz_files = glob.glob(path+"/**.npz")

    plt.rcParams["axes.prop_cycle"] = plt.cycler("color", plt.get_cmap("Set1").colors)

    d = np.load(npz_files[3])
    
    D_df = pd.DataFrame(d["arr_0"]).T
            
    ## データセットの獲得

    myds = my_SD("/data7/ALL/db/ns/8k")
    df_data = myds.concat_npz()
    df_data_pre = myds.delete_0_gyou(df_data)
    df_train,df_test = myds.train_test_dataframe(df_data=df_data_pre)

    sc = StandardScaler()
    sc.fit(df_train)
    df_train_std = pd.DataFrame(sc.transform(df_train))

    List_data_pre = df_data_pre.values.tolist()
    List_train_data = df_train.values.tolist()
    List_test_data = df_test.values.tolist
    
    ## 異常データのラベリング
    
        
    target_list = [
        "/data7/ALL/db/ns/8k/ns8k_20220609-062512.npz",
        "/data7/ALL/db/ns/8k/ns8k_20220611-020610.npz",
        "/data7/ALL/db/ns/8k/ns8k_20220611-021235.npz",
        "/data7/ALL/db/ns/8k/ns8k_20220609-063438.npz",
        "/data7/ALL/db/ns/8k/ns8k_20220609-065340.npz",]

    df_MTQ_noise = concat_npz(target_list=target_list)
    
    ## TSNEによる異常データの定義
    
    df_train_nontsne,df_tsne = myds.train_test_dataframe(df_data=df_train,frac=0.1)
    
    tsne = TSNE(random_state=0)
    List_tsne_transform = tsne.fit_transform(df_tsne)
    
    fig,ax = plt.subplots()

    ax.scatter(List_tsne_transform.T[0],List_tsne_transform.T[1])
