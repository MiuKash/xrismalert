import glob
import re
import os
import pprint

def filename_to_bucket(filename):
    """
    change filename to bucket name
    
    ------------------
    filename:string 
        (ex) tanaka_tomato1
    return:string (ex)
        (ex) tanaka
    
    """
    if "." in filename:
        Index = filename.index(".") 
        return filename[:Index]
    elif "_" in filename:
        Index = filename.index("_")
        return filename[:Index]
    elif (set(filename) & set(map(str,list(range(0,9))))) != set():
        return filename[:-1]
    return filename

if __name__ == "__main__":

    path = "/data7/ALL/db" #search directory

    file_list = [f for f in glob.glob(f"{path}/**") if os.path.isdir(f)]
    total_list = [f for f in glob.glob(f"{path}/**") if os.path.isdir(f)]
    dictionarys = dict()
    buckets = set()

    for file in file_list:

        in_file =  [f for f in os.listdir(file) if os.path.isdir(f"{file}/{f}")]
        filename=file.split("/")[-1]
        bucket = filename_to_bucket(file.split("/")[-1])

        buckets.add(bucket)
        if in_file == []:

            dictionarys[f"{filename}"]=({
                    "filename":filename,
                    "bucket":bucket,
                    "type":None,
                    #"target_measurement":
                })
        else:
            for tag in in_file:

                if "Hz" in tag:
                    total_list.append(f"{file}/{tag}")
                    dictionarys[f"{filename}_{tag}"]=({
                        "filename":filename,
                        "bucket":bucket,
                        "type":tag,
                        #"target_measurement":
                    })

                else:
                    dictionarys[f"{filename}"]=({
                        "filename":filename,
                        "bucket":bucket,
                        "type":None,
                        #"target_measurement":
                    })
                    break    

    dictionarys["psp_stat1"]["type"] = "stat1"
    dictionarys["psp_stat2"]["type"] = "stat2"
    dictionarys["psp_stat3"]["type"] = "stat3"   