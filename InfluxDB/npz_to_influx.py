import os, re,glob
import numpy as np
import pandas as pd
from influxdb_client import InfluxDBClient, Point, Dialect
from influxdb_client.client.write_api import SYNCHRONOUS, ASYNCHRONOUS
import matplotlib.pyplot as plt
from dateutil import parser
from astropy.time import Time
import pprint
import sys
args = sys.argv

def create_Points(data, measurement, timestamp,bucket, pixel):

  points = []
  freq_resolution = 12.5*10**3/2/4096

  for freq_number in range(len(list(data))):
    freq = (freq_number+1) * freq_resolution
    row = Point(measurement).tag("pixel",pixel).tag("freq",freq).field(freq, data[freq_number]).time(timestamp)
    print(timestamp)
    points.append(row)

  return points

def TO_influx(fname,bucket,target_measurement):

  """
    register npz file(fname) in influxDB
    ======================
    fname: string
      filename (e.g) /data7/ALL/db/temp/*.npz

    bucket:string
      bucketname (e.g) temp

    target_mesurement:string
      target mesurement

  """

  url = "http://localhost:8086"
  token = "9OsRFamusi7e-eaRKmHHbe6zCZRU-PDnIweRF67pNzhHSqfpxn1uNb71waAkhHTZCE96z00FVOA0PZKOsWtBQA=="
  org = "ResolveQL"

  influx_client = InfluxDBClient(url=url, token=token, org=org)
  influx_write_api = influx_client.write_api(write_options=SYNCHRONOUS)
  influx_query_api = influx_client.query_api()

  data = np.load(fname)["arr_0"]
  pixel = fname.split("_")[-2]
  time = fname.split("_")[-1].split(".")[0]

  if int(sum(np.isnan(data)*1)) == len(data):
    print("all nan")
  else:
    points=create_Points(data,target_measurement,time,bucket,pixel)
    influx_write_api.write(bucket=bucket, record=points)

  query = '''
  from(bucket:"xrismtest")
  |> range(start: 2021-07-04T09:00:00Z, stop: 2022-08-02T00:00:00Z)
  |> filter(fn: (r) => r._field =~ /CDA_TEMP0[0-9]_CAL$|IVCS5$|MVCS3$|OVCS3$|DMS3$/)
  |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
  '''


if __name__ == "__main__":

  #search_list = ["nr_ns", "ns", "pr", "nr", "wfrb","sd"]
  search_list = ["nr_ns"]

  dire_name = search_list[int(args[1])]
  path="/data7/ALL/db/"

  bucket= dire_name
  target_measurement = bucket
  dire = f"{path}{dire_name}/**/*.npz"
  f_list = sorted(glob.glob(dire,recursive=True))
    
  for fname in f_list[int(args[2]):]:
    TO_influx(fname,bucket,target_measurement)