
import os, re,glob
import numpy as np
import pandas as pd
from influxdb_client import InfluxDBClient, Point, Dialect
from influxdb_client.client.write_api import SYNCHRONOUS, ASYNCHRONOUS
import matplotlib.pyplot as plt
from dateutil import parser
from astropy.time import Time
import pprint
import sys
args = sys.argv

from xrismalert.InfluxDB.make_bucket import dictionarys, total_list


def create_Points(df_line, measurement, timestamp,bucket,type,place):
  points = []
       
  for field in df_line.keys():
    row = Point(measurement)

    if type != None:
      row = row.tag("type",type)
    if place != None:
      roe = row.tag("place",place)

    row = row.field(field, df_line[field]).time(timestamp)
    points.append(row)
  return points

def TO_influx(fname,bucket,target_measurement,type=None,place=None):
  
  """
    register picklefile(fname) in influxDB

    ======================
    fname: string
      filename (e.g.) /data7/ALL/db/psp_stat2/psp_stat2_20220213-080120_20220213-120104.pkl

    bucket:string
      bucketname (e.g.) psp

    target_mesurement:string
      target mesurement 

    type:string
      tag1 (e.g.) stat2

    place:string
    tag2 

  """

  url = "http://localhost:8086"
  token = "9OsRFamusi7e-eaRKmHHbe6zCZRU-PDnIweRF67pNzhHSqfpxn1uNb71waAkhHTZCE96z00FVOA0PZKOsWtBQA=="
  org = "ResolveQL"

  influx_client = InfluxDBClient(url=url, token=token, org=org)
  influx_write_api = influx_client.write_api(write_options=SYNCHRONOUS)
  influx_query_api = influx_client.query_api()
  
  df=pd.read_pickle(fname)
  df=df*1.0

  for time in df.index:
    name=fname.split("/")[-2]
    print(f"{name}:{time}")
    if len(df.loc[time][df.loc[time].notna()])==0:
      print("all nan")
      continue
    #timestamp=parser.parse(time)
    points=create_Points(df.loc[time][df.loc[time].notna()], target_measurement, time,bucket,type,place)
    influx_write_api.write(bucket=bucket, record=points)

  query = '''
  from(bucket:"xrismtest")
  |> range(start: 2021-07-04T09:00:00Z, stop: 2022-08-02T00:00:00Z)
  |> filter(fn: (r) => r._field =~ /CDA_TEMP0[0-9]_CAL$|IVCS5$|MVCS3$|OVCS3$|DMS3$/)
  |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
  '''


if __name__ == "__main__":

  #search_list = total_list
 
  paths = ["/data6/2207_XRISM_PFT_TC7/db/","/data7/ALL/db/"]
  search_list = ["temp","coolers","dist_pcu","fwe","psp_stat1","psp_stat2","psp_stat3","xboxa","xboxb"]

  path = paths[int(args[1])]
  filename = search_list[int(args[2])]

  bucket=dictionarys[filename]["bucket"]
  target_measurement = bucket
  type = dictionarys[filename]["type"]

  if path == paths[0]:
    place = "TC7"

  dire = f"{path}{filename}/*.pkl"
  f_list = sorted(glob.glob(dire))
    
  for fname in f_list[int(args[3]):]:
    TO_influx(fname,bucket,target_measurement,type,place)


 
  



