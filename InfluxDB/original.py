
import os, re,glob
import numpy as np
import pandas as pd
from influxdb_client import InfluxDBClient, Point, Dialect
from influxdb_client.client.write_api import SYNCHRONOUS, ASYNCHRONOUS
import matplotlib.pyplot as plt
from dateutil import parser
from astropy.time import Time
 

url = "http://localhost:8086"
token = "9OsRFamusi7e-eaRKmHHbe6zCZRU-PDnIweRF67pNzhHSqfpxn1uNb71waAkhHTZCE96z00FVOA0PZKOsWtBQA=="
org = "ResolveQL"
bucket="temp"

influx_client = InfluxDBClient(url=url, token=token, org=org)
influx_write_api = influx_client.write_api(write_options=SYNCHRONOUS)
influx_query_api = influx_client.query_api()


def create_Points(df_line, measurement, timestamp):
       points = []
       
       for field in df_line.keys():
         row = Point(measurement).field(field, df_line[field]).time(timestamp)
         points.append(row)
       return points

f_list = sorted(glob.glob("/data7/ALL/db/temp/*.pkl"))



  
df=pd.read_pickle(fname)
df=df*1
target_measurement="temp"


for time in df.index:
  print(time)
  if len(df.loc[time][df.loc[time].notna()])==0:
    print("all nan")
    continue
  #timestamp=parser.parse(time)
    points=create_Points(df.loc[time][df.loc[time].notna()], target_measurement, time)
    influx_write_api.write(bucket=bucket, record=points)

  query = '''
  from(bucket:"xrismtest")
  |> range(start: 2021-07-04T09:00:00Z, stop: 2022-08-02T00:00:00Z)
  |> filter(fn: (r) => r._field =~ /CDA_TEMP0[0-9]_CAL$|IVCS5$|MVCS3$|OVCS3$|DMS3$/)
  |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
  '''

  #df = influx_query_api.query_data_frame(query, org=org)
  #df["time"] = df["_time"]
  #df = df.drop(columns = ["_time",'result','table','_start','_stop','_measurement'])




"""

fname=f_list[2]
df=pd.read_pickle(fname)
df = df*1 #bool to int
target_measurement="temp"


for time in df.index:
  print(time)
  if len(df.loc[time][df.loc[time].notna()])==0:
    print("all nan")
    continue
  #timestamp=parser.parse(time)
  points=create_Points(df.loc[time][df.loc[time].notna()], target_measurement, time)
  influx_write_api.write(bucket=bucket, record=points)

query = '''
from(bucket:"xrismtest")
|> range(start: 2021-07-04T09:00:00Z, stop: 2022-08-02T00:00:00Z)
|> filter(fn: (r) => r._field =~ /CDA_TEMP0[0-9]_CAL$|IVCS5$|MVCS3$|OVCS3$|DMS3$/)
|> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
'''

#df = influx_query_api.query_data_frame(query, org=org)
#df["time"] = df["_time"]
#df = df.drop(columns = ["_time",'result','table','_start','_stop','_measurement'])

"""

